#
# docker build -t csabasulyok/texlive:staging .
#

FROM ubuntu:22.04

# install necessary tools
RUN apt -q update && apt install -yq wget curl perl libfontconfig python3-pip

# create working directory
RUN mkdir -p /usr/local/texlive-installer
RUN mkdir -p /usr/local/texlive
WORKDIR /usr/local/texlive-installer

# download installer
RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
    tar -xzf install-tl-unx.tar.gz && \
    rm install-tl-unx.tar.gz && \
    mv -f install-tl-* install-tl

# upload profile
COPY texlive.profile .

# run unattended installer
RUN ./install-tl/install-tl --profile=texlive.profile && \
    rm -rf ./install-tl
ENV PATH="/usr/local/texlive/bin/x86_64-linux:${PATH}"


# install texlive requirements
COPY packages.txt .
RUN /usr/local/texlive/bin/x86_64-linux/tlmgr install \
    `cat packages.txt | tr '\n' ' '`

# add pip libs
RUN pip install jsx-lexer

# remove unneccessary libraries
RUN apt -y autoremove && rm -rf /var/lib/apt/lists/*
