# TeXLive

- personal configuration of TeXLive
- package list created through export: `tlmgr info --only-installed | awk '{ print $2 }' | sed s/://g | tee packages.txt`

# Usage

## Building locally

```bash
docker build -t csabasulyok/texlive .
```

## Running

```bash
docker run -it --rm csabasulyok/texlive xelatex document.tex
```